package com.ivanovskiy.riga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class RigaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RigaApplication.class, args);
    }

}

