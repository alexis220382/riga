package com.ivanovskiy.riga.controller;

import com.ivanovskiy.riga.core.domain.Country;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.dto.CountryDto;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.CountryService;
import com.ivanovskiy.riga.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/country")

public class CountryConroler {
    @Autowired
    private CountryService countryService;

    @GetMapping(path = "/countries")
    public List<Country> getCountries() {
        return countryService.getAll();
    }

    @PostMapping(path = "")
    public Country saveCountry(@RequestBody CountryDto countryDto) {
        return countryService.save(countryDto);
    }

    @PutMapping(path = "/{id}")
    public Country updateCountry(@PathVariable("id") long id, @RequestBody CountryDto countryDto) {
        return countryService.update(id, countryDto);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteCountry(@PathVariable("id") long id) {
        countryService.delete(id);
    }
}
