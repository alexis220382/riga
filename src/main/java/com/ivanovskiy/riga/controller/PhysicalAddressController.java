package com.ivanovskiy.riga.controller;

import com.ivanovskiy.riga.core.domain.PhysicalAddress;
import com.ivanovskiy.riga.core.dto.PhysicalAddressDto;
import com.ivanovskiy.riga.core.service.PhysicalAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;



    @RestController
    @RequestMapping(path = "/address")
    public class PhysicalAddressController{

        @Autowired
        private PhysicalAddressService physicalAddressService;

        @GetMapping(path = "/addresses")
        public List<PhysicalAddress> getAddresses() {
            return physicalAddressService.getAll();
        }

        @PostMapping(path = "")
        public PhysicalAddress saveAddress(@RequestBody PhysicalAddressDto physicalAddressDto) {
            return physicalAddressService.save(physicalAddressDto);
        }

        @PutMapping(path = "/{id}")
        public PhysicalAddress updateAddress(@PathVariable("id") long id, @RequestBody PhysicalAddressDto physicalAddressDto) {
            return physicalAddressService.update(id, physicalAddressDto);
        }

        @DeleteMapping(path = "/{id}")
        public void deleteAddress(@PathVariable("id") long id) {
            physicalAddressService.delete(id);
        }
}
