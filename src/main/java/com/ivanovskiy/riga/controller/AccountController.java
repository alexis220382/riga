package com.ivanovskiy.riga.controller;

import com.ivanovskiy.riga.core.domain.BankAccount;
import com.ivanovskiy.riga.core.dto.AccountDto;
import com.ivanovskiy.riga.core.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping(path = "/accounts")
    public List<BankAccount> getAccounts() {
        return accountService.getAll();
    }

    @PostMapping(path = "")
    public BankAccount saveAccount(@RequestBody AccountDto accountDto) {
        return accountService.save(accountDto);
    }

    @PutMapping(path = "/{id}")
    public BankAccount updateAccount(@PathVariable("id") long id, @RequestBody AccountDto accountDto) {
        return accountService.update(id, accountDto);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteAccount(@PathVariable("id") long id) {
        accountService.delete(id);
    }


}
