package com.ivanovskiy.riga.controller;


import com.ivanovskiy.riga.core.domain.FitnessClub;
import com.ivanovskiy.riga.core.domain.repository.FitnessClubRepository;
import com.ivanovskiy.riga.core.dto.FitnessClubDto;
import com.ivanovskiy.riga.core.service.FitnessClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping (path = "/fitnessclub")
public class FitnessClubController {

    @Autowired
    private FitnessClubService fitnessClubService;

    @GetMapping (path = "/clubs")
    public List<FitnessClub> getFitnessClub() {
        return fitnessClubService.getAll();
    }

    @PostMapping ( path = "")
    public FitnessClub saveFitnessClub (@RequestBody FitnessClubDto fitnessClubDto) {
        return fitnessClubService.save(fitnessClubDto);
    }

    @PutMapping (path = "/{id}")
    public FitnessClub updateFitnessClub (@PathVariable ("id") long id, @RequestBody FitnessClubDto fitnessClubDto) {
        return fitnessClubService.update(id,fitnessClubDto);
    }

    @DeleteMapping (path = "/{id}")
    public void deleteFitnessClub (@PathVariable("id") long id) {
        fitnessClubService.delete(id);
    }





}
