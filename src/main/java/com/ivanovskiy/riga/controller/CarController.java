package com.ivanovskiy.riga.controller;


import com.ivanovskiy.riga.core.domain.Car;
import com.ivanovskiy.riga.core.dto.CarDto;
import com.ivanovskiy.riga.core.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/car")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping(path = "/cars")
    public List<Car> getCars() {
        return carService.getAll();
    }

    @PostMapping(path = "")
    public Car saveCar(@RequestBody CarDto carDto) {
        return carService.save(carDto);
    }

    @PutMapping(path = "/{id}")
    public Car updateCar(@PathVariable("id") long id, @RequestBody CarDto carDto) {
        return carService.update(id, carDto);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteCar(@PathVariable("id") long id) {
        carService.delete(id);
    }
}