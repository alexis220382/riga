package com.ivanovskiy.riga.controller;

import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/users")
    public List<User> getUsers() {
        return userService.getAll();
    }

    @PostMapping(path = "")
    public User saveUser(@RequestBody UserDto userDto) {
        return userService.save(userDto);
    }

    @PutMapping(path = "/{id}")
    public User updateUser(@PathVariable("id") long id, @RequestBody UserDto userDto) {
        return userService.update(id, userDto);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteUser(@PathVariable("id") long id) {
        userService.delete(id);
    }

}