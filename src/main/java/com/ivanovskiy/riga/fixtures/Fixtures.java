package com.ivanovskiy.riga.fixtures;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Fixtures {

    private final UserFixtures userFixtures;
    private final FitnessFixtures fitnessFixtures;

    private final PhysicalAddressFixtures physicalAddressFixtures;
    private final AccountFixtures accountFixtures;
    private final CountryFixture countryFixture;
    private final CarFixtures carFixtures;


    @Autowired
    public Fixtures(CarFixtures carFixtures, UserFixtures userFixtures, CountryFixture countryFixture, PhysicalAddressFixtures physicalAddressFixtures, AccountFixtures accountFixtures, FitnessFixtures fitnessFixtures) {
        this.userFixtures = userFixtures;
        this.fitnessFixtures = fitnessFixtures;
        this.physicalAddressFixtures = physicalAddressFixtures;
        this.accountFixtures = accountFixtures;
        this.countryFixture = countryFixture;
        this.carFixtures = carFixtures;

    }

    public void init() {
        log.info("Loading fixtures...");
        userFixtures.init();
        fitnessFixtures.init();
        physicalAddressFixtures.init();
        countryFixture.init();
        accountFixtures.init();

        carFixtures.init();
    }

}
