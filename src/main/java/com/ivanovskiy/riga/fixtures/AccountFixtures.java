package com.ivanovskiy.riga.fixtures;


import com.ivanovskiy.riga.core.domain.BankAccount;
import com.ivanovskiy.riga.core.domain.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountFixtures {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountFixtures(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    void init() {
        create("LV50HABA5003221312123", "SWEDBANK");
        create("LV50DNB50032213134233", "DNB");
        create("LV54HABA5003255555528", "SWEDBANK");
        create("LV10CITI5003333332123", "CITADELE");
        create("LV56PRVT5003288888888", "PRIVAT_BANK");
    }

    private void create(String accountNumber, String bankTitle) {
        BankAccount bankAccount = new BankAccount(accountNumber, bankTitle);
        accountRepository.save(bankAccount);
    }
}
