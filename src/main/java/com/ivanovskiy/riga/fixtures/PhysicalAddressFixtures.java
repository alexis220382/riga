package com.ivanovskiy.riga.fixtures;

import com.ivanovskiy.riga.core.domain.PhysicalAddress;
import com.ivanovskiy.riga.core.domain.repository.PhysicalAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhysicalAddressFixtures {

    private final PhysicalAddressRepository physicalAddressRepository;

    @Autowired
    public PhysicalAddressFixtures(PhysicalAddressRepository physicalAddressRepository) {
        this.physicalAddressRepository = physicalAddressRepository;
    }

    void init() {

        create("LV1001", "Rigas iela", 10, "LV", "Jelgava");
        create("LV1002", "Liela iela", 11, "LV", "Valmiera");
        create("LV1003", "Klusa iela", 12, "LV", "Riga");
        create("LV1004", "Raina iela", 13, "LV", "Jurmala");

    }

    private void create(String postCode, String streetName, int houseNum, String countryCode, String city) {
        PhysicalAddress physicalAddress = new PhysicalAddress(postCode, streetName, houseNum, countryCode, city);
        physicalAddressRepository.save(physicalAddress);
    }
}