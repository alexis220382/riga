package com.ivanovskiy.riga.fixtures;


import com.ivanovskiy.riga.core.domain.Car;
import com.ivanovskiy.riga.core.domain.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CarFixtures {

    private final CarRepository carRepository;

    @Autowired
    public CarFixtures(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    void init() {
        create("Volvo V70", "Black", 2003);
        create("Volkswagen Golf 4", "Silver", 1999);
        create("BMW 320i", "Blue", 2007);
        create("Hyundai i40", "White",2014);
    }

    private void create(String name, String color, int year) {
        Car car = new Car(name, color, year);
        carRepository.save(car);
    }

}
