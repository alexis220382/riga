package com.ivanovskiy.riga.fixtures;

import com.ivanovskiy.riga.core.domain.FitnessClub;
import com.ivanovskiy.riga.core.domain.repository.FitnessClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FitnessFixtures {
    private final FitnessClubRepository fitnessClubRepository;

    @Autowired
    public FitnessFixtures(FitnessClubRepository fitnessClubRepository) {
        this.fitnessClubRepository= fitnessClubRepository;
    }

    void init () {
        create("LEMON4IK", 10.00);
        create("PEOPLE FITNESS", 55.55);
        create("ANANAS", 36.66);
    }

    private void create (String name, double membershipPricePerMonth) {
        FitnessClub fitnessClub = new FitnessClub(name, membershipPricePerMonth);
        fitnessClubRepository.save(fitnessClub);
    }

}
