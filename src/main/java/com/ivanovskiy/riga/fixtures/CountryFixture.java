package com.ivanovskiy.riga.fixtures;

import com.ivanovskiy.riga.core.domain.Country;
import com.ivanovskiy.riga.core.domain.repository.CountryRepository;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component

public class CountryFixture {
    private final CountryRepository countryRepository;

    @Autowired
    public CountryFixture(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    void init() {
        create("LV", "Latvija", 1900000);
        create("LT", "Lietuva", 2800000);
        create("EE", "Igaunija", 1300000);

    }

    private void create(String country_ID, String countryName, int population) {
        Country country = new Country(country_ID, population, countryName);
        countryRepository.save(country);
    }
}

