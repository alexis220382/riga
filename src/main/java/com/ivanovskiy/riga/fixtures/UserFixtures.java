package com.ivanovskiy.riga.fixtures;

import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserFixtures {

    private final UserRepository userRepository;

    @Autowired
    public UserFixtures(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    void init() {
        create("Ilze", "Mūrnieks");
        create("Līga", "Vabule");
        create("Ivars", "Kravińš");
        create("Valdis", "Pelš");
    }

    private void create(String name, String surname) {
        User user = new User(name, surname);
        userRepository.save(user);
    }
}
