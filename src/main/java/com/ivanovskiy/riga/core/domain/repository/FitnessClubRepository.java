package com.ivanovskiy.riga.core.domain.repository;


import com.ivanovskiy.riga.core.domain.FitnessClub;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FitnessClubRepository extends CrudRepository <FitnessClub, Long> {

    @Override
    List<FitnessClub> findAll();
}
