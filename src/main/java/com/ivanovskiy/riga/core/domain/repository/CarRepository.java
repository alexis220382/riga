package com.ivanovskiy.riga.core.domain.repository;

import com.ivanovskiy.riga.core.domain.Car;
import com.ivanovskiy.riga.core.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

    @Override
    List<Car> findAll();
}
