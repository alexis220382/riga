package com.ivanovskiy.riga.core.domain;

import com.ivanovskiy.riga.core.handler.BaseEntityHandler;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
@EntityListeners(BaseEntityHandler.class)
public abstract class BaseEntity<T extends BaseEntity> {

    @Column(name = "creator")
    @NotNull
    private String creator;

    @Column(name = "creation_time")
    @NotNull
    private LocalDateTime creationTime;

    @Column(name = "modifier")
    private String modifiedBy;

    @Column(name = "modification_time")
    private LocalDateTime modificationTime;

    public BaseEntity() {

    }

    public BaseEntity(String creator, LocalDateTime creationTime, String modifiedBy, LocalDateTime modificationTime) {
        this.creator = creator;
        this.creationTime = creationTime;
        this.modifiedBy = modifiedBy;
        this.modificationTime = modificationTime;
    }
}
