package com.ivanovskiy.riga.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "user")
@NoArgsConstructor
public class User extends BaseEntity<User> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "user_id")
    private long id;

    private String name;

    private String surname;

    public User(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}
