package com.ivanovskiy.riga.core.domain.repository;

import com.ivanovskiy.riga.core.domain.PhysicalAddress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhysicalAddressRepository extends CrudRepository<PhysicalAddress,Long>
{
    @Override
    List<PhysicalAddress> findAll();
}
