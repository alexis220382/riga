package com.ivanovskiy.riga.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "fitness_club")
@NoArgsConstructor
public class FitnessClub extends BaseEntity<FitnessClub> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "fitness_club_id")
    private long id;

    private String name;
    private double membershipPricePerMonth;

    public FitnessClub(String name, double membershipPricePerMonth) {
        this.name = name;
        this.membershipPricePerMonth = membershipPricePerMonth;
    }


}
