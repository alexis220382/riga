package com.ivanovskiy.riga.core.domain.repository;

import com.ivanovskiy.riga.core.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {


    @Override
    List<User> findAll();
}
