package com.ivanovskiy.riga.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "car")
@NoArgsConstructor
public class Car extends BaseEntity<Car> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "car_id")
    private long id;

    public String name;

    public String color;

    public int year;

    public Car(String name, String color, int year) {

        this.name = name;
        this.color = color;
        this.year = year;
    }
}
