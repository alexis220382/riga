package com.ivanovskiy.riga.core.domain;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "account")
@NoArgsConstructor
public class BankAccount extends BaseEntity<BankAccount> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "account_id")
    private long id;

    private String accountNumber;
    private String bankTitle;

    public BankAccount(String bankAccount, String bankTitle) {
        this.accountNumber = bankAccount;
        this.bankTitle = bankTitle;
    }
}
