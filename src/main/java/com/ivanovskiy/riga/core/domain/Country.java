package com.ivanovskiy.riga.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "country")
@NoArgsConstructor
public class Country extends BaseEntity<Country> {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "country_ID")
    private long id;

    private String countryName;
    private int population;
    private String countryCode;

    public Country(String countryName, int population, String countryCode) {
        this.countryName = countryName;
        this.population = population;
        this.countryCode = countryCode;

    }
}
