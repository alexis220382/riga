package com.ivanovskiy.riga.core.domain.repository;

import com.ivanovskiy.riga.core.domain.BankAccount;
import com.ivanovskiy.riga.core.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<BankAccount, Long> {

    @Override
    List<BankAccount> findAll();
}
