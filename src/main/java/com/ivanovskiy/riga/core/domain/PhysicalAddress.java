package com.ivanovskiy.riga.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "address")
@NoArgsConstructor
public class PhysicalAddress extends BaseEntity<com.ivanovskiy.riga.core.domain.User> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "address_id")
    private long id;

    private String postCode;
    private String streetName;
    private int houseNum;
    private String countryCode;
    private String city;

    public PhysicalAddress(String postCode, String streetName, int houseNum, String countryCode, String city) {
        this.postCode = postCode;
        this.streetName = streetName;
        this.houseNum = houseNum;
        this.countryCode = countryCode;
        this.city = city;
    }
}
