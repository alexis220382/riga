package com.ivanovskiy.riga.core.dto;


import com.ivanovskiy.riga.core.domain.Country;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountryDto {

    private String countryName;
    private int population;
    private String countryCode;

    public CountryDto(Country country) {
        this.countryName = country.getCountryName();
        this.countryCode = country.getCountryCode();
        this.population = country.getPopulation();

    }
}
