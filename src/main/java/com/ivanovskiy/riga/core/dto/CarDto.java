package com.ivanovskiy.riga.core.dto;


import com.ivanovskiy.riga.core.domain.Car;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CarDto {

    public String name;
    public String color;
    public int year;

    public CarDto(Car car) {
        this.name = car.getName();
        this.color = car.getColor();
        this.year = car.getYear();
    }
}
