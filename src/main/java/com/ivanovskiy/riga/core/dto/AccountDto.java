package com.ivanovskiy.riga.core.dto;


import com.ivanovskiy.riga.core.domain.BankAccount;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountDto {

    private String accountNumber;
    private String bankTitle;

    public AccountDto(BankAccount bankAccount) {
        this.accountNumber = bankAccount.getAccountNumber();
        this.bankTitle = bankAccount.getBankTitle();
    }
}
