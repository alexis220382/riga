package com.ivanovskiy.riga.core.dto;

import com.ivanovskiy.riga.core.domain.PhysicalAddress;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PhysicalAddressDto {

    private String postCode;
    private String streetName;
    private int houseNum;
    private String countryCode;
    private String city;

    public PhysicalAddressDto(PhysicalAddress physicalAddress) {
        this.postCode = physicalAddress.getPostCode();
        this.streetName = physicalAddress.getStreetName();
        this.houseNum = physicalAddress.getHouseNum();
        this.countryCode = physicalAddress.getCountryCode();
        this.city = physicalAddress.getCity();
    }
}
