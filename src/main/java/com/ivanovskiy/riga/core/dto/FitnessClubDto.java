package com.ivanovskiy.riga.core.dto;


import com.ivanovskiy.riga.core.domain.FitnessClub;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FitnessClubDto {

    private String name;
    private double membershipPricePerMonth;

    public FitnessClubDto (FitnessClub fitnessClub){
        this.name = fitnessClub.getName();
        this.membershipPricePerMonth = fitnessClub.getMembershipPricePerMonth();
    }
}
