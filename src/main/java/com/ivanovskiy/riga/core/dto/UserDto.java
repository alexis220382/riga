package com.ivanovskiy.riga.core.dto;

import com.ivanovskiy.riga.core.domain.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto {

    private String name;
    private String surname;

    public UserDto(User user) {
        this.name = user.getName();
        this.surname = user.getSurname();
    }

}
