package com.ivanovskiy.riga.core.exceptions;

import com.ivanovskiy.riga.core.domain.Country;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CountryNotFoundException extends EntityNotFoundException {

    public CountryNotFoundException(String test) {
        super(test);
    }
}
