package com.ivanovskiy.riga.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends EntityNotFoundException {

    public AccountNotFoundException(String text) {
        super(text);
    }
}
