package com.ivanovskiy.riga.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CarNotFoundException extends EntityNotFoundException {
    public CarNotFoundException(String text) {
        super(text);
    }
}
