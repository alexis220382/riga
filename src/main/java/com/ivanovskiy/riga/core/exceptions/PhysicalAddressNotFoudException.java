package com.ivanovskiy.riga.core.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;
import javax.swing.text.html.parser.Entity;

@ResponseStatus(HttpStatus.NOT_FOUND)

public class PhysicalAddressNotFoudException extends EntityNotFoundException {
    public PhysicalAddressNotFoudException(String text) {
        super(text);
    }
}
