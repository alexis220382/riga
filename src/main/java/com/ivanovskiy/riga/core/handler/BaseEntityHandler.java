package com.ivanovskiy.riga.core.handler;

import com.ivanovskiy.riga.core.domain.BaseEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;


public class BaseEntityHandler {

    private BaseEntityHandler() {
    }

    @PrePersist
    public static <E extends BaseEntity> void fillBaseEntityFieldsSave(E entity) {
        entity.setCreationTime(LocalDateTime.now());
        entity.setCreator("creator");

    }

    @PreUpdate
    public static <E extends BaseEntity> void fillBaseEntityFieldsUpdate(E entity) {
        entity.setModificationTime(LocalDateTime.now());
        entity.setModifiedBy("modifier");
    }
}
