package com.ivanovskiy.riga.core.service;
import com.ivanovskiy.riga.core.domain.Car;
import com.ivanovskiy.riga.core.domain.repository.CarRepository;
import com.ivanovskiy.riga.core.dto.CarDto;
import com.ivanovskiy.riga.core.exceptions.CarNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getAll() {
        return carRepository.findAll();
    }

    public Car save(CarDto carDto) {
        Car car = new Car(carDto.getName(), carDto.getColor(), carDto.getYear());
        return carRepository.save(car);
    }

    public Car update(long id, CarDto carDto) {
        Car car = carRepository.findById(id)
                .orElseThrow(() -> new CarNotFoundException(String.format("Car with id: %d not found", id)));
        BeanUtils.copyProperties(carDto, car);
        return car;
    }

    public void delete(long id) {
        carRepository.deleteById(id);
    }
}
