package com.ivanovskiy.riga.core.service;

import com.ivanovskiy.riga.core.domain.PhysicalAddress;

import com.ivanovskiy.riga.core.domain.repository.PhysicalAddressRepository;
import com.ivanovskiy.riga.core.dto.PhysicalAddressDto;
import com.ivanovskiy.riga.core.exceptions.UserNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PhysicalAddressService {
    private final PhysicalAddressRepository physicalAddressRepository;

    @Autowired
    public PhysicalAddressService(PhysicalAddressRepository physicalAddressRepository) {
        this.physicalAddressRepository = physicalAddressRepository;
    }

    public List<PhysicalAddress> getAll() {
        return physicalAddressRepository.findAll();
    }

    public PhysicalAddress save(PhysicalAddressDto physicalAddressDto) {
        PhysicalAddress physicalAddress = new PhysicalAddress(physicalAddressDto.getPostCode(), physicalAddressDto.getStreetName(), physicalAddressDto.getHouseNum(), physicalAddressDto.getCountryCode(), physicalAddressDto.getCity());
        return physicalAddressRepository.save(physicalAddress);
    }

    public PhysicalAddress update(long id, PhysicalAddressDto physicalAddressDto) {
        PhysicalAddress physicalAddress = physicalAddressRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(String.format("Address with id: %d not found!", id)));
        BeanUtils.copyProperties(physicalAddressDto, physicalAddress);
        return physicalAddress;
    }

    public void delete(long id) {
        physicalAddressRepository.deleteById(id);
    }

}
