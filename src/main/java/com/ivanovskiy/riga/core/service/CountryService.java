package com.ivanovskiy.riga.core.service;

import com.ivanovskiy.riga.core.domain.Country;
import com.ivanovskiy.riga.core.domain.repository.CountryRepository;
import com.ivanovskiy.riga.core.dto.CountryDto;
import com.ivanovskiy.riga.core.exceptions.CountryNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAll() {
        return countryRepository.findAll();
    }

    public Country save(CountryDto countryDto) {
        Country country = new Country(countryDto.getCountryName(), countryDto.getPopulation(), countryDto.getCountryCode());
        return countryRepository.save(country);
    }

    public Country update(long id, CountryDto countryDto) {
        Country country = countryRepository.findById(id)
                .orElseThrow(() -> new CountryNotFoundException(String.format("Country with Country code: %d not found!",
                        id)));
        BeanUtils.copyProperties(countryDto, country);
        return country;
    }


    public void delete(long id) {
        countryRepository.deleteById(id);
    }
}
