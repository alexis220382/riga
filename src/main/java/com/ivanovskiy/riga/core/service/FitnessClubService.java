package com.ivanovskiy.riga.core.service;


import com.ivanovskiy.riga.core.domain.FitnessClub;
import com.ivanovskiy.riga.core.domain.repository.FitnessClubRepository;
import com.ivanovskiy.riga.core.dto.FitnessClubDto;
import com.ivanovskiy.riga.core.exceptions.FitnessClubNotFoundException;
import com.ivanovskiy.riga.core.exceptions.UserNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FitnessClubService {

    private final FitnessClubRepository fitnessClubRepository;

    @Autowired
    FitnessClubService (FitnessClubRepository fitnessClubRepository) {
        this.fitnessClubRepository = fitnessClubRepository;
    }

    public List<FitnessClub> getAll() {
        return  fitnessClubRepository.findAll();
    }

    public FitnessClub save (FitnessClubDto fitnessClubDto) {
        FitnessClub fitnessClub = new FitnessClub (fitnessClubDto.getName(), fitnessClubDto.getMembershipPricePerMonth());
        return fitnessClubRepository.save(fitnessClub);
    }

    public FitnessClub update(long id, FitnessClubDto fitnessClubDto) {
        FitnessClub fitnessClub = fitnessClubRepository.findById(id)
                .orElseThrow(()-> new FitnessClubNotFoundException(String.format("FITNESS CLUB with this id is NOT FOUND!!" , id)));
        BeanUtils.copyProperties(fitnessClubDto, fitnessClub);
        return fitnessClub;
    }

    public void delete(long id){fitnessClubRepository.deleteById(id);}


}
