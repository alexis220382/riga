package com.ivanovskiy.riga.core.service;

import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.exceptions.UserNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User save(UserDto userDto) {
        User user = new User(userDto.getName(), userDto.getSurname());
        return userRepository.save(user);
    }

    public User update(long id, UserDto userDto) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(String.format("User with id: %d not found!", id)));
        BeanUtils.copyProperties(userDto, user);
        return user;
    }


    public void delete(long id) {
        userRepository.deleteById(id);
    }

}
