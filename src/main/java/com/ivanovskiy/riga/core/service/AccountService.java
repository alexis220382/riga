package com.ivanovskiy.riga.core.service;

import com.ivanovskiy.riga.core.domain.BankAccount;
import com.ivanovskiy.riga.core.domain.repository.AccountRepository;
import com.ivanovskiy.riga.core.dto.AccountDto;
import com.ivanovskiy.riga.core.exceptions.UserNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<BankAccount> getAll() {
        return accountRepository.findAll();
    }

    public BankAccount save(AccountDto accountDto) {
        BankAccount bankAccount = new BankAccount(accountDto.getAccountNumber(), accountDto.getBankTitle());
        return accountRepository.save(bankAccount);
    }

    public BankAccount update(long id, AccountDto userDto) {
        BankAccount bankAccount = accountRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(String.format("User with id: %d not found!", id)));
        BeanUtils.copyProperties(userDto, bankAccount);
        return bankAccount;
    }

    public void delete(long id) {
        accountRepository.deleteById(id);
    }
}
