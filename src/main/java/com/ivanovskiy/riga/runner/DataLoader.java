package com.ivanovskiy.riga.runner;

import com.ivanovskiy.riga.fixtures.Fixtures;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Profile("dev")
@Component
public class DataLoader implements ApplicationRunner {

    private final Fixtures fixtures;

    @Autowired
    public DataLoader(Fixtures fixtures) {
        this.fixtures = fixtures;
    }

    @Override
    public void run(ApplicationArguments args) {
        fixtures.init();
    }

}
