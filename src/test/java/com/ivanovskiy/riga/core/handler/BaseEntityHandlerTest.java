package com.ivanovskiy.riga.core.handler;

import com.ivanovskiy.riga.core.domain.*;
import com.ivanovskiy.riga.core.domain.repository.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.ivanovskiy.riga.TestBase.*;
import static com.ivanovskiy.riga.TestBaseForCars.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BaseEntityHandlerTest {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PhysicalAddressRepository physicalAddressRepository;


    private Car testCar;
    private User testUser;
    private BankAccount testAccount;
    private PhysicalAddress testPa;

    @Autowired
    private CountryRepository countryRepository;
    private Country testCountry;


    @Before
    public void init() {
        testCar = new Car(CAR_NAME, CAR_COLOR, 2008);
        carRepository.save(testCar);
        testUser = new User(USER_NAME, USER_SURNAME);
        userRepository.save(testUser);
        testAccount = new BankAccount(ACCOUNT_NUMBER, BANK_TITLE);
        accountRepository.save(testAccount);
        testCountry = new Country(COUNTRY_NAME, COUNTRY_POPULATION, COUNTRY_ID);
        countryRepository.save(testCountry);
        testPa = new PhysicalAddress(POST_CODE, STREET_NAME, HOUSE_NUM, COUNTRY_CODE, CITY);
        physicalAddressRepository.save(testPa);

    }

    @Test
    public void fillBaseEntityFieldsSaveTest() {
        Assert.assertEquals("creator", testCar.getCreator());
        Assert.assertNotNull(testCar.getCreationTime());
        Assert.assertEquals("creator", testUser.getCreator());
        Assert.assertNotNull(testUser.getCreationTime());
        Assert.assertEquals("creator", testAccount.getCreator());
        Assert.assertNotNull(testAccount.getCreationTime());
        Assert.assertEquals("creator", testCountry.getCreator());
        Assert.assertNotNull(testCountry.getCreationTime());
        Assert.assertEquals("creator", testPa.getCreator());
        Assert.assertNotNull(testPa.getCreationTime());
    }

    @Test
    public void fillBaseEntityFieldsUpdateTest() {
        Car updatedCar = updateCar();
        Assert.assertEquals("modifier", updatedCar.getModifiedBy());
        Assert.assertNotNull(updatedCar.getModificationTime());
        User updatedUser = updateUser();

        Assert.assertEquals("modifier", updatedUser.getModifiedBy());
        Assert.assertNotNull(updatedUser.getModificationTime());

        BankAccount updatedAccount = updateAccount();

        Assert.assertEquals("modifier", updatedAccount.getModifiedBy());
        Assert.assertNotNull(updatedAccount.getModificationTime());

        Country updatedCountry = updateCountry();
        PhysicalAddress physicalAddress = updatePhysicalAddress();

        Assert.assertEquals("modifier", updatedCountry.getModifiedBy());
        Assert.assertNotNull(updatedCountry.getModificationTime());
        Assert.assertEquals("modifier", updatePhysicalAddress().getModifiedBy());
        Assert.assertNotNull(physicalAddress.getModificationTime());
    }

    @Transactional
    private Car updateCar() {
        testCar.setName(CAR_UPDATED_NAME);
        return carRepository.save(testCar);
    }

    public User updateUser() {
        testUser.setName(USER_UPDATED_NAME);
        return userRepository.save(testUser);
    }


    public BankAccount updateAccount() {
        testAccount.setAccountNumber(ACCOUNT_UPDATED_NUMBER);
        return accountRepository.save(testAccount);
    }

    public Country updateCountry() {
        testCountry.setCountryName(COUNTRY_UPDATED_COUNTRYNAME);
        return countryRepository.save(testCountry);
    }

    public PhysicalAddress updatePhysicalAddress() {
        testPa.setStreetName(UPDATE_STREET_NAME);
        return physicalAddressRepository.save(testPa);
    }


    @After
    public void cleanData() {
        carRepository.deleteAll();
        userRepository.deleteAll();
        accountRepository.deleteAll();
        physicalAddressRepository.deleteAll();
    }
}