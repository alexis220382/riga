package com.ivanovskiy.riga.core.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.Car;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.CarRepository;
import com.ivanovskiy.riga.core.dto.CarDto;
import com.ivanovskiy.riga.core.dto.UserDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ivanovskiy.riga.TestBase.UPDATE_USER;
import static com.ivanovskiy.riga.TestBase.USER_NAME;
import static com.ivanovskiy.riga.TestBase.USER_SURNAME;
import static com.ivanovskiy.riga.TestBaseForCars.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarServiceTest {

    @MockBean
    private CarRepository carRepository;

    @Autowired
    private CarService carService;

    @Autowired
    private ObjectMapper objectMapper;

    private Car testCar;

    @Before
    public void init() {
        testCar = new Car(CAR_NAME, CAR_COLOR,2009);
    }

    @Test
    public void getAllTest() {
        Mockito.when(carRepository.findAll()).thenReturn(Collections.singletonList(testCar));

        List<Car> cars = carService.getAll();

        Assert.assertFalse(cars.isEmpty());
        Assert.assertEquals(1, cars.stream().filter(it -> it.getName().equals(CAR_NAME)).count());
        Assert.assertEquals(1, cars.stream().filter(it ->it.getColor().equals(CAR_COLOR)).count());

    }

    @Test
    public void saveTest() {
        Mockito.when(carRepository.save(Mockito.any())).thenReturn(testCar);

        Car car = carService.save(new CarDto(testCar));

        Assert.assertEquals(CAR_NAME, car.getName());
        Assert.assertEquals(CAR_COLOR, car.getColor());
    }

    @Test
    public void updateTest() throws IOException {
        Mockito.when(carRepository.findById(testCar.getId())).thenReturn(Optional.ofNullable(testCar));

        CarDto carDto = objectMapper.readValue(UPDATE_CAR, new TypeReference<CarDto>() {
        });
        Car car = carService.update(testCar.getId(), carDto);

        Assert.assertEquals("Updated name", car.getName());
        Assert.assertEquals("Updated color", car.getColor());
    }
}