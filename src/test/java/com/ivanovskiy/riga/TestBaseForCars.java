package com.ivanovskiy.riga;

public class TestBaseForCars {

    public static final String CAR_NAME = "Hyundai i40";
    public static final String CAR_UPDATED_NAME = "Audi A6";
    public static final String CAR_COLOR = "Red";

    public static final String NEW_CAR =
            "{" +
                    "\"name\":\"Name\"," +
                    "\"color\":\"Color\"" +
                    "}";

    public static final String UPDATE_CAR =
            "{" +
                    "\"name\":\"Updated name\"," +
                    "\"color\":\"Updated color\"" +
                    "}";
}
