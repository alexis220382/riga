package com.ivanovskiy.riga;

public class TestBase {

    public static final String USER_NAME = "Valdis";
    public static final String USER_UPDATED_NAME = "Ivars";
    public static final String USER_SURNAME = "Grieziņš";

    public static final String CLUB_NAME = "Limon";
    public static final String CLUB_UPDATED_NAME = "Olymp";
    public static final double CLUB_PRICE = 40.50;

    public static final String ACCOUNT_NUMBER = "LV50HABA5003221312123";
    public static final String ACCOUNT_UPDATED_NUMBER = "LV50HABA5003221312188";
    public static final String BANK_TITLE = "SWEDBANK";


    public static final String POST_CODE = "LV1999";
    public static final String STREET_NAME = "Pirma iela";
    public static final String UPDATE_STREET_NAME = "Liela iela";
    public static final String COUNTRY_CODE = "LV";
    public static final String CITY = "Varaklani";
    public static final int HOUSE_NUM = 1;

    public static final String COUNTRY_NAME = "ASV";
    public static final String COUNTRY_UPDATED_COUNTRYNAME = "Canada";
    public static final String COUNTRY_ID = "CAN";
    public static final int COUNTRY_POPULATION = 999999;



    public static final String NEW_USER =
            "{" +
                    "\"name\":\"Name\"," +
                    "\"surname\":\"Surname\"" +
                    "}";

    public static final String UPDATE_USER =
            "{" +
                    "\"name\":\"Updated name\"," +
                    "\"surname\":\"Updated surname\"" +
                    "}";

    public static final String NEW_CLUB =
            "{" +
                 "\"name\":\"Limon\"," +
                 "\"membershipPricePerMonth\":40.50" +
            "}";

    public static final String UPDATE_CLUB =
            "{" +
                 "\"name\":\"Olymp\"," +
                 "\"membershipPricePerMonth\":40.50" +
            "}";

    public static final String NEW_ACCOUNT =
            "{" +
                    "\"accountNumber\":\"AccountNumber\"," +
                    "\"bankTitle\":\"BankTitle\"" +
                    "}";

    public static final String UPDATE_ACCOUNT =
            "{" +
                    "\"accountNumber\":\"Updated accountNumber\"," +
                    "\"bankTitle\":\"Updated bankTitle\"" +
                    "}";

    public static final String NEW_ADDRESS =
            "{" +
                    "\"postCode\":\"PostCode\"," +
                    "\"streetName\":\"StreetName\"," +
                    "\"houseNum\": 1," +
                    "\"countryCode\":\"CountryCode\"," +
                    "\"city\":\"City\"" +
                    "}";

    public static final String UPDATE_ADDRESS =
            "{" +
                    "\"postCode\":\"Updated PostCode\"," +
                    "\"streetName\":\"Updated StreetName\"," +
                    "\"houseNum\":2," +
                    "\"countryCode\":\"Updated CountryCode\"," +
                    "\"city\":\"Updated City\"" +

                    "}";

    public static final String NEW_COUNTRY =
            "{" +
                    "\"countryName\":\"CountryName\"," +
                    "\"population\":10000000," +
                    "\"countryCode\":\"CountryName\"" +
                    "}";
    public static final String UPDATE_COUNTRY =
            "{" +
                    "\"countryName\":\"Updated country Name\"," +
                    "\"population\":10000000," +
                    "\"countryCode\":\"Updated countryCode\"" +
                    "}";

}

