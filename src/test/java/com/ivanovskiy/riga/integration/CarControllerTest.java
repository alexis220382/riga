package com.ivanovskiy.riga.integration;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.Car;
import com.ivanovskiy.riga.core.domain.repository.CarRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;
import java.util.List;

import static com.ivanovskiy.riga.TestBaseForCars.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CarRepository carRepository;

    private Car testCar;

    @Before
    public void init() {
        testCar = new Car(CAR_NAME, CAR_COLOR, 2008);
        carRepository.save(testCar);

    }

    @Test
    public void getCarsTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/car/cars")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        List<Car> list = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Car>>() {
        });
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void saveCarTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/car")
                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_CAR))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Car car = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<Car>() {

        });

        assert car != null;
        Assert.assertEquals("Name", car.getName());
    }

    @Test
    public void updateCarTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/car/" + testCar.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(UPDATE_CAR))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Car car = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<Car>() {
        });

        assert car != null;
        Assert.assertEquals("Updated name", car.getName());
    }

    @Test
    public void deleteCarTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/car/" + testCar.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
    }

    @After
    public void cleanData() {
        carRepository.deleteAll();
    }


}
