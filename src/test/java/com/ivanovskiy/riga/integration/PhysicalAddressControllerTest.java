package com.ivanovskiy.riga.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.PhysicalAddress;
import com.ivanovskiy.riga.core.domain.repository.PhysicalAddressRepository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static com.ivanovskiy.riga.TestBase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PhysicalAddressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PhysicalAddressRepository physicalAddressRepository;

    private PhysicalAddress testPa;

    @Before
    public void init() {
        testPa = new PhysicalAddress(POST_CODE, STREET_NAME, HOUSE_NUM, COUNTRY_CODE, CITY);
        physicalAddressRepository.save(testPa);
    }

    @Test
    public void getAddressesTest() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/address/addresses")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();
        List<PhysicalAddress> list = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<PhysicalAddress>>() {
        });
        Assert.assertEquals(1, list.size());

    }

    @Test
    public void saveAddressTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/address")

                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_ADDRESS))
                .andExpect(status().is2xxSuccessful()).andReturn();


        PhysicalAddress physicalAddress = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<PhysicalAddress>() {
        });

        assert physicalAddress != null;
        Assert.assertEquals("PostCode", physicalAddress.getPostCode());
        Assert.assertEquals("City", physicalAddress.getCity());
        Assert.assertEquals(1, physicalAddress.getHouseNum());
        Assert.assertEquals("CountryCode", physicalAddress.getCountryCode());
        Assert.assertEquals("StreetName", physicalAddress.getStreetName());
    }

    @Test
    public void updatePhysicalAddressTest() throws Exception {

        MvcResult mvcResult = mockMvc.perform(put("/address/" + testPa.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(UPDATE_ADDRESS))
                .andExpect(status().is2xxSuccessful()).andReturn();

        PhysicalAddress physicalAddress = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<PhysicalAddress>() {

        });


        assert physicalAddress != null;
        Assert.assertEquals("Updated StreetName", physicalAddress.getStreetName());
        Assert.assertEquals("Updated City", physicalAddress.getCity());
        Assert.assertEquals("Updated CountryCode", physicalAddress.getCountryCode());
        Assert.assertEquals("Updated PostCode", physicalAddress.getPostCode());
        Assert.assertEquals(2, physicalAddress.getHouseNum());
    }


    @Test
    public void deletePhysicalAddressTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/address/" + testPa.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
    }

    @After
    public void cleanData() {
        physicalAddressRepository.deleteAll();
    }

}