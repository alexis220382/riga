package com.ivanovskiy.riga.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.FitnessClub;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.FitnessClubRepository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static com.ivanovskiy.riga.TestBase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class FitnessClubControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FitnessClubRepository fitnessClubRepository;

    private FitnessClub testFitnessClub;

    @Before
    public void init() {
        testFitnessClub = new FitnessClub (CLUB_NAME, CLUB_PRICE);
        fitnessClubRepository.save(testFitnessClub);
    }

    @Test
    public void getFitnessClubsTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/fitnessclub/clubs")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful()).andReturn();

        List<FitnessClub> list = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<FitnessClub>>() {
        });

        Assert.assertEquals(1, list.size());
    }

    @Test
    public void saveFitnessClubTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/fitnessclub")
                .contentType(MediaType.APPLICATION_JSON)
        .content(NEW_CLUB))
        .andExpect(status().is2xxSuccessful()).andReturn();

        FitnessClub fitnessClub = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<FitnessClub>() {
        });

        assert fitnessClub != null;
        Assert.assertEquals("Limon", fitnessClub.getName());
    }

    @Test
    public void updateFitnessClubTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/fitnessclub/" + testFitnessClub.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(UPDATE_CLUB))
        .andExpect(status().is2xxSuccessful()).andReturn();

        FitnessClub fitnessClub = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<FitnessClub>() {
        });

        assert fitnessClub != null;
        Assert.assertEquals("Olymp", fitnessClub.getName());
    }

    @Test
    public void deleteFitnessClubTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/fitnessclub/" + testFitnessClub.getId())
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
    }

    @After
    public void cleanData() {
        fitnessClubRepository.deleteAll();
    }
}
