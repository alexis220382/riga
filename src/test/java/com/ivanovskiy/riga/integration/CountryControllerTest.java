package com.ivanovskiy.riga.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.Country;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.CountryRepository;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static com.ivanovskiy.riga.TestBase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")

public class CountryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CountryRepository countryRepository;

    private Country testCountry;

    @Before
    public void init() {
        testCountry = new Country(COUNTRY_NAME, COUNTRY_POPULATION, COUNTRY_ID);
        countryRepository.save(testCountry);
    }

    @Test
    public void getCountriesTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/country/countries")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        List<Country> list = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Country>>() {
        });

        Assert.assertEquals(1, list.size());
    }

    @Test
    public void saveUserTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/country")
                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_COUNTRY))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Country country = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<Country>() {
        });

        assert country != null;
        Assert.assertEquals("CountryName", country.getCountryName());
    }

    @Test
    public void updateCountryTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/country/" + testCountry.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(UPDATE_COUNTRY))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Country country = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<Country>() {
        });

        assert country != null;
        Assert.assertEquals("Updated country Name", country.getCountryName());
    }

    @Test
    public void deleteUserTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/country/" + testCountry.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
    }

    @After
    public void cleanData() {
        countryRepository.deleteAll();
    }

}
