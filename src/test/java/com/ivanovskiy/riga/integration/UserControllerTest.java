package com.ivanovskiy.riga.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static com.ivanovskiy.riga.TestBase.*;
import static com.ivanovskiy.riga.TestBase.USER_SURNAME;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    private User testUser;

    @Before
    public void init() {
        testUser = new User(USER_NAME, USER_SURNAME);
        userRepository.save(testUser);
    }

    @Test
    public void getUsersTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/user/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        List<User> list = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<User>>() {
        });

        Assert.assertEquals(1, list.size());
    }

    @Test
    public void saveUserTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_USER))
                .andExpect(status().is2xxSuccessful()).andReturn();

        User user = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<User>() {
        });

        assert user != null;
        Assert.assertEquals("Name", user.getName());
    }

    @Test
    public void updateUserTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/user/" + testUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(UPDATE_USER))
                .andExpect(status().is2xxSuccessful()).andReturn();

        User user = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<User>() {
        });

        assert user != null;
        Assert.assertEquals("Updated name", user.getName());
    }

    @Test
    public void deleteUserTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/user/" + testUser.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
    }

    @After
    public void cleanData() {
        userRepository.deleteAll();
    }
}
