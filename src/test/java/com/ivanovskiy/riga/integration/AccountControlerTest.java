package com.ivanovskiy.riga.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.BankAccount;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.AccountRepository;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static com.ivanovskiy.riga.TestBase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")

public class AccountControlerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountRepository accountRepository;

    private BankAccount testAccount;

    @Before
    public void init() {
        testAccount = new BankAccount(ACCOUNT_NUMBER, BANK_TITLE);
        accountRepository.save(testAccount);
    }

    @Test
    public void getAccountTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/account/accounts")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        List<BankAccount> list = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<BankAccount>>() {
        });

        Assert.assertEquals(1, list.size());
    }

    @Test
    public void saveAccountTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_ACCOUNT))
                .andExpect(status().is2xxSuccessful()).andReturn();

        BankAccount bankAccount = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<BankAccount>() {
        });

        assert bankAccount != null;
        Assert.assertEquals("AccountNumber", bankAccount.getAccountNumber());
    }

    @Test
    public void updateAccountTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/account/" + testAccount.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(UPDATE_ACCOUNT))
                .andExpect(status().is2xxSuccessful()).andReturn();

        BankAccount account = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<BankAccount>() {
        });

        assert account != null;
        Assert.assertEquals("Updated accountNumber", account.getAccountNumber());
    }

    @Test
    public void deleteAccountTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/account/" + testAccount.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
    }

    @After
    public void cleanData() {
        accountRepository.deleteAll();
    }
}

