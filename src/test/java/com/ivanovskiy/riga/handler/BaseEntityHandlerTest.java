package com.ivanovskiy.riga.handler;

import com.ivanovskiy.riga.core.domain.*;

import com.ivanovskiy.riga.core.domain.repository.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.ivanovskiy.riga.TestBase.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BaseEntityHandlerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FitnessClubRepository fitnessClubRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PhysicalAddressRepository physicalAddressRepository;

    @Autowired
    private CountryRepository countryRepository;

    private User testUser;
    private FitnessClub testFitnessClub;
    private BankAccount testAccount;
    private PhysicalAddress testPa;
    private Country testCountry;

    @Before
    public void init() {
        testUser = new User(USER_NAME, USER_SURNAME);
        userRepository.save(testUser);
        testFitnessClub = new FitnessClub(CLUB_NAME, CLUB_PRICE);
        fitnessClubRepository.save(testFitnessClub);
        testAccount = new BankAccount(ACCOUNT_NUMBER, BANK_TITLE);
        accountRepository.save(testAccount);
        testPa = new PhysicalAddress(POST_CODE, STREET_NAME, HOUSE_NUM, COUNTRY_CODE, CITY);
        physicalAddressRepository.save(testPa);
        testCountry = new Country(COUNTRY_NAME, COUNTRY_POPULATION, COUNTRY_ID);
        countryRepository.save(testCountry);

    }

    @Test
    public void fillBaseEntityFieldsSaveTest() {
        Assert.assertEquals("creator", testUser.getCreator());
        Assert.assertNotNull(testUser.getCreationTime());
        Assert.assertNull(testUser.getModifiedBy());
        Assert.assertNull(testUser.getModificationTime());

        Assert.assertEquals("creator", testFitnessClub.getCreator());
        Assert.assertNotNull(testFitnessClub.getCreationTime());
        Assert.assertNull(testFitnessClub.getModifiedBy());
        Assert.assertNull(testFitnessClub.getModificationTime());

        Assert.assertEquals("creator", testAccount.getCreator());
        Assert.assertNotNull(testAccount.getCreationTime());
        Assert.assertNull(testAccount.getModifiedBy());
        Assert.assertNull(testAccount.getModificationTime());

        Assert.assertEquals("creator", testPa.getCreator());
        Assert.assertNotNull(testPa.getCreationTime());
        Assert.assertNull(testPa.getModifiedBy());
        Assert.assertNull(testPa.getModificationTime());

        Assert.assertEquals("creator", testCountry.getCreator());
        Assert.assertNotNull(testCountry.getCreationTime());
        Assert.assertNull(testCountry.getModifiedBy());
        Assert.assertNull(testCountry.getModificationTime());
    }

    @Test
    public void fillBaseEntityFieldsUpdateTest() {
        User updatedUser = updateUser();

        Assert.assertEquals("creator", updatedUser.getCreator());
        Assert.assertNotNull(updatedUser.getCreationTime());
        Assert.assertEquals("modifier", updatedUser.getModifiedBy());
        Assert.assertNotNull(updatedUser.getModificationTime());

        FitnessClub updateFitnessClub = updateFitnessClub();

        Assert.assertEquals("creator", updateFitnessClub.getCreator());
        Assert.assertNotNull(updateFitnessClub.getCreationTime());
        Assert.assertEquals("modifier", updateFitnessClub.getModifiedBy());
        Assert.assertNotNull(updateFitnessClub.getModificationTime());

        BankAccount updatedAccount = updateAccount();

        Assert.assertEquals("creator", updatedAccount.getCreator());
        Assert.assertNotNull(updatedAccount.getCreationTime());
        Assert.assertEquals("modifier", updatedAccount.getModifiedBy());
        Assert.assertNotNull(updatedAccount.getModificationTime());

        PhysicalAddress physicalAddress = updatePhysicalAddress();

        Assert.assertEquals("creator", updatePhysicalAddress().getCreator());
        Assert.assertNotNull(physicalAddress.getCreationTime());
        Assert.assertEquals("modifier", updatePhysicalAddress().getModifiedBy());
        Assert.assertNotNull(physicalAddress.getModificationTime());

        Country updatedCountry = updateCountry();

        Assert.assertEquals("creator", updatedCountry.getCreator());
        Assert.assertNotNull(updatedCountry.getCreationTime());
        Assert.assertEquals("modifier", updatedCountry.getModifiedBy());
        Assert.assertNotNull(updatedCountry.getModificationTime());
    }

    @Transactional
    public User updateUser() {
        testUser.setName(USER_UPDATED_NAME);
        return userRepository.save(testUser);
    }

    @Transactional
    public FitnessClub updateFitnessClub() {
        testFitnessClub.setName(CLUB_UPDATED_NAME);
        return fitnessClubRepository.save(testFitnessClub);
    }

    @Transactional
    public BankAccount updateAccount() {
        testAccount.setAccountNumber(ACCOUNT_UPDATED_NUMBER);
        return accountRepository.save(testAccount);
    }

    @Transactional
    public PhysicalAddress updatePhysicalAddress() {
        testPa.setStreetName(UPDATE_STREET_NAME);
        return physicalAddressRepository.save(testPa);
    }

    @Transactional
    public Country updateCountry() {
        testCountry.setCountryName(COUNTRY_UPDATED_COUNTRYNAME);
        return countryRepository.save(testCountry);
    }

    @After
    public void cleanData() {
        userRepository.deleteAll();
        fitnessClubRepository.deleteAll();
    }
}
