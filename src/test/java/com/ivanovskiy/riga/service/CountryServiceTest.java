package com.ivanovskiy.riga.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.Country;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.CountryRepository;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import com.ivanovskiy.riga.core.dto.CountryDto;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.CountryService;
import com.ivanovskiy.riga.core.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ivanovskiy.riga.TestBase.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryServiceTest {
    @MockBean
    private CountryRepository countryRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ObjectMapper objectMapper;

    private Country testCountry;

    @Before
    public void init() {
        testCountry = new Country(COUNTRY_NAME, COUNTRY_POPULATION, COUNTRY_ID);
    }

    @Test
    public void getAllTest() {
        Mockito.when(countryRepository.findAll()).thenReturn(Collections.singletonList(testCountry));

        List<Country> countries = countryService.getAll();

        Assert.assertFalse(countries.isEmpty());
        Assert.assertEquals(1, countries.stream().filter(it -> it.getCountryName().equals(COUNTRY_NAME)).count());
        Assert.assertEquals(1, countries.stream().filter(it -> it.getCountryCode().equals(COUNTRY_ID)).count());
    }

    @Test
    public void saveTest() {
        Mockito.when(countryRepository.save(Mockito.any())).thenReturn(testCountry);

        Country country = countryService.save(new CountryDto(testCountry));

        Assert.assertEquals(COUNTRY_NAME, country.getCountryName());
        Assert.assertEquals(COUNTRY_ID, country.getCountryCode());
    }

    @Test
    public void updateTest() throws IOException {
        Mockito.when(countryRepository.findById(testCountry.getId())).thenReturn(Optional.ofNullable(testCountry));

        CountryDto countryDto = objectMapper.readValue(UPDATE_COUNTRY, new TypeReference<CountryDto>() {
        });
        Country country = countryService.update(testCountry.getId(), countryDto);

        Assert.assertEquals("Updated country Name", country.getCountryName());
        Assert.assertEquals("Updated countryCode", country.getCountryCode());
    }


}
