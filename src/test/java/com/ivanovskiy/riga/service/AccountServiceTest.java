package com.ivanovskiy.riga.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.BankAccount;
import com.ivanovskiy.riga.core.domain.repository.AccountRepository;
import com.ivanovskiy.riga.core.dto.AccountDto;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.AccountService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ivanovskiy.riga.TestBase.*;

@RunWith(SpringRunner.class)
@SpringBootTest

public class AccountServiceTest {

    @MockBean
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ObjectMapper objectMapper;

    private BankAccount testAccount;

    @Before
    public void init() {
        testAccount = new BankAccount(ACCOUNT_NUMBER, BANK_TITLE);
    }

    @Test
    public void getAllTest() {
        Mockito.when(accountRepository.findAll()).thenReturn(Collections.singletonList(testAccount));

        List<BankAccount> accounts = accountService.getAll();

        Assert.assertFalse(accounts.isEmpty());
        Assert.assertEquals(1, accounts.stream().filter(it -> it.getAccountNumber().equals(ACCOUNT_NUMBER)).count());
        Assert.assertEquals(1, accounts.stream().filter(it -> it.getBankTitle().equals(BANK_TITLE)).count());
    }

    @Test
    public void saveTest() {
        Mockito.when(accountRepository.save(Mockito.any())).thenReturn(testAccount);

        BankAccount account = accountService.save(new AccountDto(testAccount));

        Assert.assertEquals(ACCOUNT_NUMBER, account.getAccountNumber());
        Assert.assertEquals(BANK_TITLE, account.getBankTitle());
    }

    @Test
    public void updateTest() throws IOException {
        Mockito.when(accountRepository.findById(testAccount.getId())).thenReturn(Optional.ofNullable(testAccount));

        AccountDto accountDto = objectMapper.readValue(UPDATE_ACCOUNT, new TypeReference<AccountDto>() {
        });
        BankAccount account = accountService.update(testAccount.getId(), accountDto);

        Assert.assertEquals("Updated accountNumber", account.getAccountNumber());
        Assert.assertEquals("Updated bankTitle", account.getBankTitle());
    }
}