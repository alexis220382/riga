package com.ivanovskiy.riga.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ivanovskiy.riga.TestBase.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    private User testUser;

    @Before
    public void init() {
        testUser = new User(USER_NAME, USER_SURNAME);
    }

    @Test
    public void getAllTest() {
        Mockito.when(userRepository.findAll()).thenReturn(Collections.singletonList(testUser));

        List<User> users = userService.getAll();

        Assert.assertFalse(users.isEmpty());
        Assert.assertEquals(1, users.stream().filter(it -> it.getName().equals(USER_NAME)).count());
        Assert.assertEquals(1, users.stream().filter(it -> it.getSurname().equals(USER_SURNAME)).count());
    }

    @Test
    public void saveTest() {
        Mockito.when(userRepository.save(Mockito.any())).thenReturn(testUser);

        User user = userService.save(new UserDto(testUser));

        Assert.assertEquals(USER_NAME, user.getName());
        Assert.assertEquals(USER_SURNAME, user.getSurname());
    }

    @Test
    public void updateTest() throws IOException {
        Mockito.when(userRepository.findById(testUser.getId())).thenReturn(Optional.ofNullable(testUser));

        UserDto userDto = objectMapper.readValue(UPDATE_USER, new TypeReference<UserDto>() {
        });
        User user = userService.update(testUser.getId(), userDto);

        Assert.assertEquals("Updated name", user.getName());
        Assert.assertEquals("Updated surname", user.getSurname());
    }

}
