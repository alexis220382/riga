package com.ivanovskiy.riga.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.PhysicalAddress;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.PhysicalAddressRepository;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import com.ivanovskiy.riga.core.dto.PhysicalAddressDto;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.PhysicalAddressService;
import com.ivanovskiy.riga.core.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ivanovskiy.riga.TestBase.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhysicalAddressServiceTest {
    @MockBean
    private PhysicalAddressRepository physicalAddressRepository;

    @Autowired
    private PhysicalAddressService physicalAddressService;

    @Autowired
    private ObjectMapper objectMapper;

    private PhysicalAddress testPa;

    @Before
    public void init() {
        testPa = new PhysicalAddress(POST_CODE, STREET_NAME, HOUSE_NUM, COUNTRY_CODE, CITY);

    }

    @Test
    public void getAll() {
        Mockito.when(physicalAddressRepository.findAll()).thenReturn(Collections.singletonList(testPa));
        List<PhysicalAddress> physicalAddresses = physicalAddressService.getAll();

        Assert.assertFalse(physicalAddresses.isEmpty());
        Assert.assertEquals(1, physicalAddresses.stream().filter(it -> it.getCity().equals(CITY)).count());
        Assert.assertEquals(1, physicalAddresses.stream().filter(it -> it.getCountryCode().equals(COUNTRY_CODE)).count());
        Assert.assertEquals(1, physicalAddresses.stream().filter(it -> it.getHouseNum() == HOUSE_NUM).count());
        Assert.assertEquals(1, physicalAddresses.stream().filter(it -> it.getPostCode().equals(POST_CODE)).count());
        Assert.assertEquals(1, physicalAddresses.stream().filter(it -> it.getStreetName().equals(STREET_NAME)).count());

    }

    @Test
    public void save() {
        Mockito.when(physicalAddressRepository.save(Mockito.any())).thenReturn(testPa);

        PhysicalAddress physicalAddress = physicalAddressService.save(new PhysicalAddressDto(testPa));


        Assert.assertEquals(POST_CODE, physicalAddress.getPostCode());
        Assert.assertEquals(CITY, physicalAddress.getCity());
        Assert.assertEquals(HOUSE_NUM, physicalAddress.getHouseNum());
        Assert.assertEquals(COUNTRY_CODE, physicalAddress.getCountryCode());
        Assert.assertEquals(STREET_NAME, physicalAddress.getStreetName());

    }

    @Test
    public void update() throws IOException {
        Mockito.when(physicalAddressRepository.findById(testPa.getId())).thenReturn(Optional.ofNullable(testPa));

        PhysicalAddressDto physicalAddressDto = objectMapper.readValue(UPDATE_ADDRESS, new TypeReference<PhysicalAddressDto>() {
        });

        PhysicalAddress physicalAddress = physicalAddressService.update(testPa.getId(), physicalAddressDto);

        Assert.assertEquals("Updated StreetName", physicalAddress.getStreetName());
        Assert.assertEquals("Updated City", physicalAddress.getCity());
        Assert.assertEquals("Updated CountryCode", physicalAddress.getCountryCode());
        Assert.assertEquals("Updated PostCode", physicalAddress.getPostCode());
        Assert.assertEquals(2, physicalAddress.getHouseNum());


    }

    @Test
    public void delete() {
    }
}