package com.ivanovskiy.riga.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanovskiy.riga.core.domain.FitnessClub;
import com.ivanovskiy.riga.core.domain.User;
import com.ivanovskiy.riga.core.domain.repository.FitnessClubRepository;
import com.ivanovskiy.riga.core.domain.repository.UserRepository;
import com.ivanovskiy.riga.core.dto.FitnessClubDto;
import com.ivanovskiy.riga.core.dto.UserDto;
import com.ivanovskiy.riga.core.service.FitnessClubService;
import com.ivanovskiy.riga.core.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.attribute.FileTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ivanovskiy.riga.TestBase.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FitnessClubServiceTest {

    @MockBean
    private FitnessClubRepository fitnessClubRepository;

    @Autowired
    private FitnessClubService fitnessClubService;

    @Autowired
    private ObjectMapper objectMapper;

    private FitnessClub testFitnessClub;

    @Before
    public void init() {
        testFitnessClub = new FitnessClub(CLUB_NAME, CLUB_PRICE);
    }

    @Test
    public void getAllTest() {
        Mockito.when(fitnessClubRepository.findAll()).thenReturn(Collections.singletonList(testFitnessClub));

        List<FitnessClub> fitnessClubs = fitnessClubService.getAll();

        Assert.assertFalse(fitnessClubs.isEmpty());
        Assert.assertEquals(1, fitnessClubs.stream().filter(it -> it.getName().equals(CLUB_NAME)).count());
        Assert.assertEquals(1, fitnessClubs.stream().filter(it -> it.getMembershipPricePerMonth() == (CLUB_PRICE)).count());
    }

    @Test
    public void saveTest() {
        Mockito.when(fitnessClubRepository.save(Mockito.any())).thenReturn(testFitnessClub);

        FitnessClub fitnessClub = fitnessClubService.save(new FitnessClubDto(testFitnessClub));

        Assert.assertEquals(CLUB_NAME, fitnessClub.getName());
        Assert.assertTrue(CLUB_PRICE == fitnessClub.getMembershipPricePerMonth());
    }

    @Test
    public void updateTest() throws IOException {
        Mockito.when(fitnessClubRepository.findById(testFitnessClub.getId())).thenReturn(Optional.ofNullable(testFitnessClub));

        FitnessClubDto fitnessClubDto = objectMapper.readValue(UPDATE_CLUB, new TypeReference<FitnessClubDto>() {
        });
        FitnessClub fitnessClub = fitnessClubService.update(testFitnessClub.getId(), fitnessClubDto);

        Assert.assertEquals("Olymp", fitnessClub.getName());
        Assert.assertTrue(40.50 == fitnessClub.getMembershipPricePerMonth());
    }

}
